# goal of this module is to make an interactive periodic table

import dash
import dash_html_components as html
import pandas as pd

# https://code.sololearn.com/WdIW9uI7egdv/#html 

def generate_html_table():
    return html.Table(
        children=[
            # body
            html.Tr(
                html.Td(
                    html.A('H')
                ),
                html.Td(
                )
            )
        ]
        # Header
    )

app = dash.Dash(__name__, )

app.layout = html.Div(children=[
    html.H4(children='Periodic Table'),
    generate_html_table()
])

if __name__ == '__main__':
    app.run_server(debug=True)