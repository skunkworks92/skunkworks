import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd
import plotly.io as pio
import numpy as np
import plotly.graph_objects as go
import pickle
# import dataframe

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# PLOTLY STYLINGS
# https://plotly.com/python/styling-plotly-express/
# pio.templates.default = "plotly_dark"

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# Find the sourcefile
import pandas as pd
material_system_sheet = 'NiTi.xlsx'
print('Source file {} found.'.format(material_system_sheet))
print('Beginning construction of pandas dataframe')

niti_raw = open('niti.pkl', 'rb')
foo = pickle.load(niti_raw)
niti_clean = open('niti_clean.pkl', 'rb')
skunk = pickle.load(niti_clean)
elements = pd.ExcelFile(material_system_sheet)
print('NiTi.xlsx source data has created:  niti_df')

# Test to make sure the dataframe worked
# foo['Ag'].head()

# Get Elements
def get_elements():
    clean_elements = elements.sheet_names
    # Make a try except 
    clean_elements.remove('Calculators')# drop unneeded elements
    print('Available element worksheets are:')
    for element in clean_elements:
        print(element)
    return clean_elements

available_elements = get_elements()

# We are going to need to get columns for every element
def get_dataframe_columns(element, df):
    columns = df[element].columns
    for column in columns:
        print(column)
    return columns

def sanitize_columns(element, df):
    df[element].columns = df[element].columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(', '').str.replace(')', '').str.replace('-', '').str.replace('%','').str.replace('°','').str.replace('#','').str.replace('ε','')
    return(df[element].columns)

for element in available_elements:
    sanitize_columns(element, foo)

# In foo df we need to change the value of column L to x_element
for element in available_elements:
    x_element = element.lower()
    foo[element] = foo[element].rename(columns = {x_element:'x_element'})
    print('{} dataframe has changed column name to x_element'.format(element))

foo_columns = get_dataframe_columns('Ag', foo)

# Do the same for skunk
for element in available_elements:
    x_element = element
    skunk[element] = skunk[element].rename(columns = {x_element:'X_element'})
    print('{} dataframe has changed column name to x_element'.format(element))

skunk_columns = get_dataframe_columns('Ag', skunk)

def build_column_dict(foo, bar):
    # Make a dictionary to be able to look up the value for the key that skunk provides
    column_dict = {skunk_columns[i]: foo_columns[i] for i in range(len(foo_columns))}
    return column_dict

# test=build_column_dict(foo_columns, skunk_columns)
# test.get('Austenite Finish Temperature - AF - (°C)')
# test.get('Applied Stress - σApplied - (MPa) ')
# foo['Ag'].applied_stress__σapplied__mpa

columns = build_column_dict(foo_columns, skunk_columns)

# PRESANITATION DATA CAPTURE
# 1.) Know the proper data formats before they are destroyed by sanitation
def get_axis(df):
    plotable_axis = list(df['Ag'].columns)
    # ToDo: Put the below 3 lines into a try so it doesn't fail if they are in there or not
    remove = ['Notes', 'Ref #', 'Year', 'I.', 'P. ','Authors', 'Atomic Formulas','Title', 'Alloy Processing Summary','Source', 'V.', 'Unnamed: 66', 'Unnamed: 67','Unnamed: 68', 'Unnamed: 69', 'Unnamed: 70', 'Prep Mtd.', 'Test Direction', 'Test Type(s)']
    for column in remove:
        try:
            plotable_axis.remove(column)
        except:
            print('The column {} is tagged for removal, but not present in the criteria'.format(column))
    return plotable_axis

# 2.) Execute data capture
axis = get_axis(skunk)
print(axis)   

# let's figure out checklists
test_types = list(foo['Hf'].test_types.dropna().unique())
test_directions = list(foo['Hf'].test_direction.dropna().unique())
prep_mtd = list(foo['Hf'].prep_mtd.dropna().unique())
for number, test in enumerate(test_types):
    print(number, test)

# APP CODE
plot_types = [ 'Scatter', 'Ternary', 'Box-and-Whisker', 'Sunburst']
app.layout = html.Div([
    html.H1("SCATTER PLOT MODULE"),
    html.H2('NiTi Dataframe'),
    html.Div([
            "ELEMENT: ",
                dcc.Dropdown(
                id='element-selection',
                options=[{'label': i, 'value': i} for i in available_elements],
                value=available_elements[3]
            ),
            html.Br(),
            "X-AXIS",
                dcc.Dropdown(
                id='x-axis',
                options=[{'label': i, 'value': i} for i in axis],
                value=axis[0]
                ),
            html.Br(),
            "Y-AXIS",
                dcc.Dropdown(
                id='y-axis',
                options=[{'label': i, 'value': i} for i in axis],
                value=axis[1]
                ),
            html.Br(),
            "COLORBAR",
                dcc.Dropdown(
                id='colorbar-selection',
                options=[{'label': i, 'value': i} for i in axis],
                value=axis[2]
                ),
            html.Br(),
            "Plot Type",
                dcc.Dropdown(
                    id='plot-selection',
                    options=[{'label': i, 'value': i} for i in plot_types],
                    value=plot_types[0]
                ),
            html.Br(),
            "Test Types",
                 dcc.Checklist(id='test-type-checklist',
                    options=[{'label': i, 'value': i} for i in test_types],
                    value=test_types),
            html.Br(),
            "Test Directions",
                 dcc.Checklist(id='test-direction-checklist',
                    options=[{'label': i, 'value': i} for i in test_directions],
                    value=test_directions),
            html.Br(),
            "Prep Mtd",
                 dcc.Checklist(id='prep-mtd-checklist',
                    options=[{'label': i, 'value': i} for i in prep_mtd],
                    value=prep_mtd),
    ]),
    html.Br(),
    # This is a test that Dash can read my damn dataframe
    html.Br(),
    html.H1(id='my-output'),
    # html.H3(id='dataframe-output'),
    html.P(id='dataframe-output'),
    html.Div([dcc.Graph(id='plot')], 
    )
])

def find_unchecked_test_type(test_list):
    test_type_original = test_types
    result = set(test_type_original).difference(test_list)
    print("Missing Test Types:", (set(test_type_original).difference(test_list))) 
    return result

def find_unchecked_test_direction(test_direction_list):
    test_direction_original = test_directions
    result = (set(test_direction_original).difference(test_direction_list))
    print("Missing Test Directions: ", (set(test_direction_original).difference(test_direction_list)))
    return result

def find_unchecked_prep_mtd(prep_mtd_list):
    prep_mtd_original = prep_mtd
    result = (set(prep_mtd_original).difference(prep_mtd_list))
    print("Missing Prep: ", (set(prep_mtd_original).difference(prep_mtd_list)))
    return result


# ELEMENTS BEING PLOTTED
@app.callback(
    Output(component_id='my-output', component_property='children'),
    [Input(component_id='element-selection', component_property='value')],
)
def update_output_div(input_value):
    return input_value

# ELEMENTS AND AXIS TO CHOOSE DATAFRAME DATA
@app.callback(
    Output('plot', 'figure'),
    [Input(component_id='element-selection', component_property='value'),
     Input(component_id='x-axis', component_property='value'),
     Input(component_id='y-axis', component_property='value'),
     Input(component_id='colorbar-selection', component_property='value'),
     Input(component_id='plot-selection', component_property='value'),
     Input(component_id='test-type-checklist', component_property='value'),
     Input(component_id='test-direction-checklist', component_property='value'),
     Input(component_id='prep-mtd-checklist', component_property='value')])
# def update_dataframe_div(element_selection, x_axis_selection, y_axis_selection):
#     return element_selection, x_axis_selection, y_axis_selection

def update_plot(element_selection, x_axis_selection, y_axis_selection, colorbar_selection, plot_selection, test_type_checklist, test_direction_checklist, prep_mtd_checklist):
    df = foo[element_selection]
    
    test_type_filter = find_unchecked_test_type(test_type_checklist)
    test_direction_filter = find_unchecked_test_direction(test_direction_checklist)
    prep_mtd_filter = find_unchecked_prep_mtd(prep_mtd_checklist)
    # find what test_type is missing
    x_axis = columns.get(x_axis_selection)
    # print(x_axis)
    y_axis = columns.get(y_axis_selection)
    # print(y_axis)
    color = columns.get(colorbar_selection)
    
    if plot_selection == 'Scatter':
        px.defaults.template = "plotly_white"
        px.defaults.color_continuous_scale = px.colors.sequential.Blackbody
        # px.defaults.width = 1000
        px.defaults.height = 900
        print('Scatter plot has been chosen to display data')
        fig = px.scatter(df,
                        x=df[x_axis],
                        y=df[y_axis],
                        hover_name=df.title,
                        color=df[color]
                        )
        
        fig.update_xaxes(title=x_axis_selection)

        fig.update_yaxes(title=y_axis_selection)

        # https://plotly.com/python/marker-style/
        fig.update_traces(marker=dict(size=12,
                                line=dict(width=1,
                                            color='DarkSlateGrey')),
                                            selector=dict(mode='markers')) 

        fig.update_layout(
            font=dict(
                        family="Courier New, monospace",
                        size=24,
                        color="RebeccaPurple"
                    )
        )

        return fig

    if plot_selection == 'Ternary':
        px.defaults.template = "plotly_white"
        px.defaults.color_continuous_scale = px.colors.sequential.Blackbody
        # px.defaults.width = 1000
        px.defaults.height = 900
        print("Ternary Plot has been chosen to disoplay data")
        fig = px.scatter_ternary(df,
                        a=df['x_element'],
                        b=df['ni'],
                        c=df['ti'],
                        hover_name=df.title,
                        color=df[color]
                        )
        
        fig.update_layout(
                font=dict(
                        family="Courier New, monospace",
                        size=24,
                        color="RebeccaPurple"
                    )
                )
        fig.update_ternaries(aaxis_title=dict(text=element_selection), baxis_title=dict(text='Ni'), caxis_title=dict(text='Ti'),  )
        fig.update_traces(marker=dict(size=12,
                                line=dict(width=1,
                                            color='DarkSlateGrey')),
                                            selector=dict(mode='markers'))

        return fig

    if plot_selection == 'Box-and-Whisker':
        print("Box and whisker plot has been chosen to disoplay data")
        # fig = px.box(

        N = 30     # Number of boxes

        # generate an array of rainbow colors by fixing the saturation and lightness of the HSL
        # representation of colour and marching around the hue.
        # Plotly accepts any CSS color format, see e.g. http://www.w3schools.com/cssref/css_colors_legal.asp.
        c = ['hsl('+str(h)+',50%'+',50%)' for h in np.linspace(0, 360, N)]

        # Each box is represented by a dict that contains the data, the type, and the colour.
        # Use list comprehension to describe N boxes, each with a different colour and with different randomly generated data:
        fig = go.Figure(data=[go.Box(
            y=3.5 * np.sin(np.pi * i/N) + i/N + (1.5 + 0.5 * np.cos(np.pi*i/N)) * np.random.rand(10),
            marker_color=c[i]
            ) for i in range(int(N))])

        # format the layout
        fig.update_layout(
                xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                yaxis=dict(zeroline=False, gridcolor='white'),
                paper_bgcolor='rgb(233,233,233)',
                plot_bgcolor='rgb(233,233,233)',
            )

        return fig
        #                 df,
        #                 # a = element
        #                 # b = Ni column
        #                 # c = Ti column
        #                 y=df[y_axis],
        #                 hover_name=df.title,
        #                 color=df[color]
        #                 )
        
        # # fig.update_xaxes(title=x_axis_selection)

        # # fig.update_yaxes(title=y_axis_selection)

        # # https://plotly.com/python/marker-style/
        # # fig.update_traces(marker=dict(size=12,
        # #                         line=dict(width=2,
        # #                                     color='DarkSlateGrey')),
        # #                 selector=dict(mode='markers')) 

        # return fig

if __name__ == '__main__':
    app.run_server(debug=True)
# %%
