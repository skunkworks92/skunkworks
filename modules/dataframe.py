#!/usr/bin/env python
# coding: utf-8

# In[189]:


# Find the sourcefile
import pandas as pd
material_system_sheet = 'NiTi.xlsx'
print('Source file {} found.'.format(material_system_sheet))
print('Beginning construction of pandas dataframe')


# In[190]:


# Build the dataframe
# workaround for all the nasty sanitation needed
foo = pd.read_excel(material_system_sheet, skiprows=5, sheet_name=None) # foo wil be the dataframe with the sanitation of data
skunk = pd.read_excel(material_system_sheet, skiprows=5, sheet_name=None) # skunk will be the maintained dataframe
elements = pd.ExcelFile(material_system_sheet)
print('NiTi.xlsx source data has created:  niti_df')


# In[191]:


# Test to make sure the dataframe worked
foo['Ag'].head()


# In[192]:


# Get Elements
def get_elements():
    clean_elements = elements.sheet_names
    # Make a try except 
    clean_elements.remove('Calculators')# drop unneeded elements
    print('Available element worksheets are:')
    for element in clean_elements:
        print(element)
    return clean_elements


# In[193]:


available_elements = get_elements()


# In[194]:


# We are going to need to get columns for every element
def get_dataframe_columns(element, df):
    columns = df[element].columns
    for column in columns:
        print(column)
    return columns


# In[195]:


skunk_columns = get_dataframe_columns('Ag', skunk)


# In[196]:


foo_columns = skunk_columns


# In[197]:


# DATAFRAME COLUMN SANITATION

# pd doesn't like spaces 
# pd doesn't like characters
# pd doesn't like UPPERCASE
# pd doesn't like units

# for every element I want to sanitize these columns for the data frame
def sanitize_columns(element, df):
    df[element].columns = df[element].columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(', '').str.replace(')', '').str.replace('-', '').str.replace('%','').str.replace('°','').str.replace('#','').str.replace('ε','')

    return(df[element].columns)


# In[198]:


for element in available_elements:
    sanitize_columns(element, foo)


# In[199]:


foo_columns = foo['Ag'].columns
print(foo_columns)


# In[200]:


skunk_columns = skunk['Ag'].columns
print(skunk_columns)


# In[201]:


def build_column_dict(foo, bar):
    # Make a dictionary to be able to look up the value for the key that skunk provides
    column_dict = {skunk_columns[i]: foo_columns[i] for i in range(len(foo_columns))}
    return column_dict


# In[202]:


test=build_column_dict(foo_columns, skunk_columns)


# In[203]:


test.get('Austenite Finish Temperature - AF - (°C)')


# In[204]:


test.get('Applied Stress - σApplied - (MPa) ')


# In[205]:


foo['Ag'].applied_stress__σapplied__mpa


# In[ ]:




